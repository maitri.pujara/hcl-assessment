package com.hcl.test;

import static org.junit.Assert.assertEquals;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;

import javax.naming.directory.InvalidAttributesException;

import org.junit.BeforeClass;
import org.junit.Test;
import com.hcl.controller.ApplicationController;

public class ApplicationTest {

	static ApplicationController controller;
	private static ByteArrayOutputStream outContent;

	@BeforeClass
	public static void initialize() {
		controller = new ApplicationController();
		outContent = new ByteArrayOutputStream();
		System.setOut(new PrintStream(outContent));
	}

	@Test
	public void testCalculateTriangleArea() {

		int height = 3;
		int base = 5;

		try {
			float area = controller.calculateTriangleArea(base, height);
			assertEquals(7.5f, area, 0);

		} catch (InvalidAttributesException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	@Test(expected = InvalidAttributesException.class)
	public void testCalculateTriangleAreaWithZero() throws InvalidAttributesException {

		int height = 3;
		int base = 0;

		controller.calculateTriangleArea(base, height);


	}

	@Test
	public void testPrintNumbers() {

		controller.printNumbers();
		assertEquals(new String("1 2 3 4 5 6 7 8 9 10 "), outContent.toString());
	}
}
