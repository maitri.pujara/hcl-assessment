package com.hcl.controller;

import javax.naming.directory.InvalidAttributesException;

import com.hcl.service.PrinterService;
import com.hcl.service.TriangleAreaCalculatorService;


/**
 * The Class ApplicationController.
 */
public class ApplicationController {

	/**
	 * Calculate triangle area with base and height.
	 *
	 * @param base   the base
	 * @param height the height
	 * @return the float
	 * @throws InvalidAttributesException 
	 */
	public float calculateTriangleArea(int base, int height) throws InvalidAttributesException {

		return TriangleAreaCalculatorService.calculateArea(base, height);
	}

	/**
	 * Prints the numbers.
	 */
	public void printNumbers() {

		PrinterService.printNumbers();

	}

	/**
	 * Prints the triangle area.
	 *
	 * @param area the area
	 */
	public void printTriangleArea(float area) {

		PrinterService.printTriangleArea(area);

	}
}
