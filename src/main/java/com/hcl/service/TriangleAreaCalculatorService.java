package com.hcl.service;

import javax.naming.directory.InvalidAttributesException;

import com.hcl.functionalInterface.TriangleAreaWithBaseAndHeight;

/**
 * The Class TriangleAreaCalculatorService.
 */
public class TriangleAreaCalculatorService {
	
	/** Calculate area of triangle using base and height as per following formula area = (base * height) / 2. */
	private static TriangleAreaWithBaseAndHeight calcBaseHeight = (int base, int height) -> (base * height) / 2.0f;

	/**
	 * Calculate area.
	 *
	 * @param base the base
	 * @param height the height
	 * @return the float
	 * @throws InvalidAttributesException 
	 */
	public static float calculateArea(int base, int height) throws InvalidAttributesException {
		if(base>0 && height>0)
			return calcBaseHeight.calculate(base, height);
		else
			throw new InvalidAttributesException("base and height must be positive");
		
	}

}
