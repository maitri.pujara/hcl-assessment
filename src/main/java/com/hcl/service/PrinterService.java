package com.hcl.service;

import java.util.stream.IntStream;

/**
 * The Class PrinterService.
 */
public class PrinterService {

	/**
	 * Prints the triangle area.
	 *
	 * @param area the area
	 */
	public static void printTriangleArea(float area) {
		System.out.println("Area of the Triangle is :  " + area);
	}

	/**
	 * Prints the numbers.
	 */
	public static void printNumbers() {
		IntStream.rangeClosed(1, 10).forEach(n->
		{
			System.out.print(n+" ");
		});
	}

}
