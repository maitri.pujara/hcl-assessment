package com.hcl.main;

import java.util.InputMismatchException;
import java.util.Scanner;

import javax.naming.directory.InvalidAttributesException;

import com.hcl.controller.ApplicationController;

/**
 * The Class ApplicationMain.
 */
public class ApplicationMain {

	/**
	 * The main method.
	 *
	 * @param args the arguments
	 * @throws InvalidAttributesException 
	 */
	public static void main(String[] args) throws InvalidAttributesException {

		try (Scanner scanner = new Scanner(System.in)) {
			ApplicationController controller = new ApplicationController();
			String continueTest = "N";
			do {
				System.out.println(
						"To calculate the area of triangle, press 1 OR to print the numbers 1 to 10, press any other number :");
				int option = scanner.nextInt();
				if (option == 1) {

					System.out.println("Enter base of the triangle.");
					int base = scanner.nextInt();

					System.out.println("Enter height of the triangle.");
					int height = scanner.nextInt();

					if (base <= 0 || height <= 0) {
						System.out.println("Please enter base and height > 0.");
						System.out.println("Do you want to continue testing? Enter Y for yes or enter any other key.");
						continueTest = scanner.next();
						continue;
					}
					float area = controller.calculateTriangleArea(base, height);
					controller.printTriangleArea(area);
					System.out.println("Do you want to continue testing? Enter Y for yes or enter any other key.");
					continueTest = scanner.next();

				} else {
					controller.printNumbers();
					System.out.println("Do you want to continue testing? Enter Y for yes or enter any other key.");
					continueTest = scanner.next();
				}
			} while (continueTest.equalsIgnoreCase("Y"));
			System.out.println("Thank You!");
			System.exit(0);
		}catch(InputMismatchException in) {
			System.out.println("Please enter valid input.");
		}

	}

}
