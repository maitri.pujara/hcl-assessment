package com.hcl.functionalInterface;


/**
 * The Interface TriangleAreaWithBaseAndHeight.
 */
@FunctionalInterface
public interface TriangleAreaWithBaseAndHeight {

	/**
	 * Calculate.
	 *
	 * @param base the base
	 * @param height the height
	 * @return the float
	 */
	float calculate(int base, int height);

}
