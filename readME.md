**Code Assessment**

> 	This project has 2 functionalities. 1) Calculate area of a triangle with given base and height 2) Display numbers 1 to 10 on console.
	
**How to run**

Go to project directory where you clone it.
Open CMD. 
run the command:

> mvn clean install

> java -jar target\code-assessment-0.0.1-SNAPSHOT.jar
				   
System takes command-line arguments to make the program easy to test. Read the instructions displayed on CMD and provide the input accordingly. Only positive values are accepted for triangle area calculation. 

Git URL contains the pipeline with 2 stages : Build & Test

Note: Source code is implemented using Java 8. 

	
	